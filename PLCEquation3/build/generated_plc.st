FUNCTION_BLOCK Ladder
  VAR_INPUT
    a : BOOL;
    b : BOOL;
    c : BOOL;
    de : BOOL;
    ee : BOOL;
  END_VAR
  VAR_OUTPUT
    E0 : BOOL := 1;
    E1 : BOOL;
    E2 : BOOL;
    E3 : BOOL;
  END_VAR
  VAR
    T1 : BOOL;
    T2 : BOOL;
    T3 : BOOL;
    T4 : BOOL;
    C1 : BOOL;
    CTU1 : CTU;
    R_TRIG1 : R_TRIG;
  END_VAR

  T1 := a AND E0;
  R_TRIG1(CLK := E3);
  CTU1(CU := R_TRIG1.Q, R := E0, PV := INT#3);
  C1 := CTU1.Q;
  T2 := b AND E1;
  T3 := E2 AND c;
  T4 := NOT(C1) AND E3 AND de;
  E0 := NOT(T1) AND (ee AND C1 OR E0);
  E1 := NOT(T2) AND (E1 OR T1 OR T4);
  E2 := NOT(T3) AND (E2 OR E2 OR T2 OR T2);
  E3 := NOT(C1) AND NOT(T4) AND (E3 OR T3);
END_FUNCTION_BLOCK

PROGRAM Programa1
  VAR
    a : BOOL := 1;
    b : BOOL := 1;
    c : BOOL := 1;
    de : BOOL := 1;
    Ladder0 : Ladder;
    E0 : Led;
    E1 : Led;
    E2 : Led;
    Button0 : Button;
    Button1 : Button;
    Button2 : Button;
    Button3 : Button;
    Button4 : Button;
    E3 : Led;
  END_VAR

  Button0(back_id := 'A_OFF', sele_id := 'A_ON', toggle := a);
  Button1(back_id := 'B_OFF', sele_id := 'B_ON', toggle := b);
  Button2(back_id := 'C_OFF', sele_id := 'C_ON', toggle := c);
  Button3(back_id := 'D_OFF', sele_id := 'D_ON', toggle := de);
  Button4(back_id := 'E_OFF', sele_id := 'E_ON', toggle := de);
  Ladder0(a := Button0.state_out, b := Button1.state_out, c := Button2.state_out, de := Button3.state_out, ee := Button4.state_out);
  E0(back_id := 'E0_OFF', sele_id := 'E0_ON', state_in := Ladder0.E0);
  E1(back_id := 'E1_OFF', sele_id := 'E1_ON', state_in := Ladder0.E1);
  E2(back_id := 'E2_OFF', sele_id := 'E2_ON', state_in := Ladder0.E2);
  E3(back_id := 'E3_OFF', sele_id := 'E3_ON', state_in := Ladder0.E3);
END_PROGRAM


CONFIGURATION config

  RESOURCE resource1 ON PLC
    TASK Prueba3(INTERVAL := T#200ms,PRIORITY := 0);
    PROGRAM Instancia1 WITH Prueba3 : Programa1;
  END_RESOURCE
END_CONFIGURATION
