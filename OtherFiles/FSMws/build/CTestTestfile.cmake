# CMake generated Testfile for 
# Source directory: /home/netza/FSMws/src
# Build directory: /home/netza/FSMws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(rosserial/rosserial)
subdirs(rosserial/rosserial_arduino)
subdirs(rosserial/rosserial_mbed)
subdirs(rosserial/rosserial_msgs)
subdirs(rosserial/rosserial_python)
subdirs(rosserial/rosserial_tivac)
subdirs(rosserial/rosserial_xbee)
subdirs(rosserial/rosserial_client)
subdirs(rosserial/rosserial_server)
subdirs(FSMNet)
subdirs(rosserial/rosserial_embeddedlinux)
subdirs(rosserial/rosserial_test)
subdirs(rosserial/rosserial_windows)
