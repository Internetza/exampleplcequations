# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/usr/share/arduinoo/hardware/arduino/cores/arduino/WInterrupts.c" "/home/netza/FSMws/build/FSMNet/src/CMakeFiles/uno_CORE.dir/usr/share/arduinoo/hardware/arduino/cores/arduino/WInterrupts.c.obj"
  "/usr/share/arduinoo/hardware/arduino/cores/arduino/avr-libc/malloc.c" "/home/netza/FSMws/build/FSMNet/src/CMakeFiles/uno_CORE.dir/usr/share/arduinoo/hardware/arduino/cores/arduino/avr-libc/malloc.c.obj"
  "/usr/share/arduinoo/hardware/arduino/cores/arduino/avr-libc/realloc.c" "/home/netza/FSMws/build/FSMNet/src/CMakeFiles/uno_CORE.dir/usr/share/arduinoo/hardware/arduino/cores/arduino/avr-libc/realloc.c.obj"
  "/usr/share/arduinoo/hardware/arduino/cores/arduino/wiring.c" "/home/netza/FSMws/build/FSMNet/src/CMakeFiles/uno_CORE.dir/usr/share/arduinoo/hardware/arduino/cores/arduino/wiring.c.obj"
  "/usr/share/arduinoo/hardware/arduino/cores/arduino/wiring_analog.c" "/home/netza/FSMws/build/FSMNet/src/CMakeFiles/uno_CORE.dir/usr/share/arduinoo/hardware/arduino/cores/arduino/wiring_analog.c.obj"
  "/usr/share/arduinoo/hardware/arduino/cores/arduino/wiring_digital.c" "/home/netza/FSMws/build/FSMNet/src/CMakeFiles/uno_CORE.dir/usr/share/arduinoo/hardware/arduino/cores/arduino/wiring_digital.c.obj"
  "/usr/share/arduinoo/hardware/arduino/cores/arduino/wiring_pulse.c" "/home/netza/FSMws/build/FSMNet/src/CMakeFiles/uno_CORE.dir/usr/share/arduinoo/hardware/arduino/cores/arduino/wiring_pulse.c.obj"
  "/usr/share/arduinoo/hardware/arduino/cores/arduino/wiring_shift.c" "/home/netza/FSMws/build/FSMNet/src/CMakeFiles/uno_CORE.dir/usr/share/arduinoo/hardware/arduino/cores/arduino/wiring_shift.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "USB_CON"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/netza/FSMws/build/FSMNet/ros_lib"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/usr/share/arduinoo/hardware/arduino/cores/arduino/CDC.cpp" "/home/netza/FSMws/build/FSMNet/src/CMakeFiles/uno_CORE.dir/usr/share/arduinoo/hardware/arduino/cores/arduino/CDC.cpp.obj"
  "/usr/share/arduinoo/hardware/arduino/cores/arduino/HID.cpp" "/home/netza/FSMws/build/FSMNet/src/CMakeFiles/uno_CORE.dir/usr/share/arduinoo/hardware/arduino/cores/arduino/HID.cpp.obj"
  "/usr/share/arduinoo/hardware/arduino/cores/arduino/HardwareSerial.cpp" "/home/netza/FSMws/build/FSMNet/src/CMakeFiles/uno_CORE.dir/usr/share/arduinoo/hardware/arduino/cores/arduino/HardwareSerial.cpp.obj"
  "/usr/share/arduinoo/hardware/arduino/cores/arduino/IPAddress.cpp" "/home/netza/FSMws/build/FSMNet/src/CMakeFiles/uno_CORE.dir/usr/share/arduinoo/hardware/arduino/cores/arduino/IPAddress.cpp.obj"
  "/usr/share/arduinoo/hardware/arduino/cores/arduino/Print.cpp" "/home/netza/FSMws/build/FSMNet/src/CMakeFiles/uno_CORE.dir/usr/share/arduinoo/hardware/arduino/cores/arduino/Print.cpp.obj"
  "/usr/share/arduinoo/hardware/arduino/cores/arduino/Stream.cpp" "/home/netza/FSMws/build/FSMNet/src/CMakeFiles/uno_CORE.dir/usr/share/arduinoo/hardware/arduino/cores/arduino/Stream.cpp.obj"
  "/usr/share/arduinoo/hardware/arduino/cores/arduino/Tone.cpp" "/home/netza/FSMws/build/FSMNet/src/CMakeFiles/uno_CORE.dir/usr/share/arduinoo/hardware/arduino/cores/arduino/Tone.cpp.obj"
  "/usr/share/arduinoo/hardware/arduino/cores/arduino/USBCore.cpp" "/home/netza/FSMws/build/FSMNet/src/CMakeFiles/uno_CORE.dir/usr/share/arduinoo/hardware/arduino/cores/arduino/USBCore.cpp.obj"
  "/usr/share/arduinoo/hardware/arduino/cores/arduino/WMath.cpp" "/home/netza/FSMws/build/FSMNet/src/CMakeFiles/uno_CORE.dir/usr/share/arduinoo/hardware/arduino/cores/arduino/WMath.cpp.obj"
  "/usr/share/arduinoo/hardware/arduino/cores/arduino/WString.cpp" "/home/netza/FSMws/build/FSMNet/src/CMakeFiles/uno_CORE.dir/usr/share/arduinoo/hardware/arduino/cores/arduino/WString.cpp.obj"
  "/usr/share/arduinoo/hardware/arduino/cores/arduino/main.cpp" "/home/netza/FSMws/build/FSMNet/src/CMakeFiles/uno_CORE.dir/usr/share/arduinoo/hardware/arduino/cores/arduino/main.cpp.obj"
  "/usr/share/arduinoo/hardware/arduino/cores/arduino/new.cpp" "/home/netza/FSMws/build/FSMNet/src/CMakeFiles/uno_CORE.dir/usr/share/arduinoo/hardware/arduino/cores/arduino/new.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "USB_CON"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/netza/FSMws/build/FSMNet/ros_lib"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
