#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/netza/FSMws/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/netza/FSMws/devel/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/netza/FSMws/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD="/home/netza/FSMws/build"
export PYTHONPATH="/home/netza/FSMws/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/netza/FSMws/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/netza/FSMws/src:$ROS_PACKAGE_PATH"