#include <ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Int8.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float64.h>
#include <Arduino.h>
ros::NodeHandle nh;
std_msgs::String str_msg;
std_msgs::Int8 Estado;
std_msgs::Int32 Tiempo;
std_msgs::Float64 Real;
ros::Publisher chatter("chatter", &str_msg);
ros::Publisher estadoss("Estados", &Estado);
ros::Publisher tiempoo("Tiempos",&Tiempo);
ros::Publisher Reall("Real",&Real);
#define SENSOR (PIND & B00001100)>>2
#define SALIDA PORTB
struct EstadoS{
	unsigned long (*Salidas)(void);
	unsigned long Retardo;
	double Real;
        char *mensaje;
	unsigned long ProximoEstado[4];};
typedef const struct EstadoS Miestado;
unsigned long VNRE(void){SALIDA = 0x21;};
unsigned long ANRE(){SALIDA = 0x22;};
unsigned long RNVE(){SALIDA = 0x0C;};
unsigned long RNAE(){SALIDA = 0x14;};
#define Nav 0
#define Nesp 1
#define Eav 2
#define Eesp 3
Miestado FSM[4] = {
	{&VNRE, 1000, 1.12, "Norte Avanza 100 001", {Nav, Nesp, Nav, Nesp}},
	{&ANRE, 3000, 2.13, "Norte Espera 100 010", {Eav, Eav, Eav, Eav}},
	{&RNVE, 1000, 3.14, "Este Avanza 001 100", {Eav, Eav, Eesp, Eesp}},
	{&RNAE, 3000, 4.15, "Este Espera 010 100", {Nav, Nav, Nav, Nav}}};
unsigned long E, Entrada;
void setup()
{
 DDRB = B11111111;
 DDRD = 0x00;
 nh.initNode();
 nh.advertise(chatter);
 nh.advertise(estadoss);
 nh.advertise(tiempoo);
 nh.advertise(Reall);
}
void loop()
{
E=Nav;
delay(FSM[E].Retardo);
Entrada = SENSOR;
E = FSM[E].ProximoEstado[Entrada];
Estado.data = SENSOR;
str_msg.data = FSM[E].mensaje;
Tiempo.data = FSM[E].Retardo;
Real.data = FSM[E].Real;
chatter.publish(&str_msg);
estadoss.publish(&Estado);
tiempoo.publish(&Tiempo);
Reall.publish(&Real);
nh.spinOnce();
//delay(1000);
}
