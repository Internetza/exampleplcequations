//#include <cv.h>
//#include <highgui.h>
//#include <iostream>
#include "opencv2/core/core_c.h"
#include "opencv2/core/core.hpp"
#include "opencv2/flann/miniflann.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/video.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/ml/ml.hpp"
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/contrib/contrib.hpp"
using namespace cv;
using namespace std;
int main(int argc,char** argv )
{
double alpha = 0.5;
double beta;
double input;
Mat src1, src2, dst;
/// Ask the user enter alpha
cout<<"Simple Linear Blender"<<endl;
cout<<"-----------------------"<<endl;
cout<<"*Enter alpha [0-1]:";
cin>>input;
/// We use the alpha provided by the user if it is between 0 and 1
if( input >= 0.0 && input <= 1.0)
{ alpha = input; }
/// Read image ( same size, same type )
src1 = imread("OpenCVLogo.png");
src2 = imread("OpenCVLogo.png");
if(!src1.data ) { cout<<"Error loading src1"<<endl;
return -1; }
if(!src2.data ) { cout<<"Error loading src2"<<endl;
return -1; }
/// Create Windows
namedWindow("Linear Blend",1);
beta = (1.0-alpha );
addWeighted( src1, alpha, src2, beta,0.0, dst);
imshow("Linear Blend", dst );
waitKey(0);
return 0;
}
