//Eroding filtrado de imagen
#include "opencv2/core/core_c.h"
#include "opencv2/core/core.hpp"
#include "opencv2/flann/miniflann.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/video.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/ml/ml.hpp"
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/contrib/contrib.hpp"
//

//#include "opencv2/highgui/stdafx.h"
//#include "stdafx.h"
//#include "opencv2/highgui/cv.h"
//#include <cv.h>
//#include <highgui.h>
//#include "opencv2/highgui/highgui.hpp"
#include <iostream>

int main()
{
        //display the original image
//        IplImage* img = cvLoadImage("C:/MyPic.jpg");
        IplImage* img = cvLoadImage("OpenCVLogo.png");
        cvNamedWindow("MyWindow");
        cvShowImage("MyWindow", img);

        //erode and display the eroded image
        cvErode(img, img, 0, 2);
        cvNamedWindow("Eroded");
        cvShowImage("Eroded", img);
       
        cvWaitKey(0);
       
        //cleaning up
        cvDestroyWindow("MyWindow");
        cvDestroyWindow("Eroded");
        cvReleaseImage(&img);
       
        return 0;
} 
