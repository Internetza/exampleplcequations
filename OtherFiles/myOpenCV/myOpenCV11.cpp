//Filtro negativo inverting an image
#include "opencv2/core/core_c.h"
#include "opencv2/core/core.hpp"
#include "opencv2/flann/miniflann.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/video.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/ml/ml.hpp"
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/contrib/contrib.hpp"


//#include "stdafx.h"/
//#include <cv.h>
//#include <highgui.h>

int main()
{
        //display the original image
//        IplImage* img = cvLoadImage("C:/MyPic.jpg");
	IplImage* img = cvLoadImage("OpenCVLogo.png");
        cvNamedWindow("MyWindow");
        cvShowImage("MyWindow", img);

        //invert and display the inverted image
        cvNot(img, img);
        cvNamedWindow("Inverted");
        cvShowImage("Inverted", img);

        cvWaitKey(0);
      
        //cleaning up
        cvDestroyWindow("MyWindow");
        cvDestroyWindow("Inverted");
        cvReleaseImage(&img);
      
        return 0;
}
